﻿Imports System.Data.SqlClient
Public Class frmInicio
    Public counter As Integer

    Const INTERVALO_EN_MINUTOS As Integer = 140

    Private Sub Form1_Load(sender As Object, e As EventArgs)
        Timer1.Enabled = True
        Timer1.Interval = 1000

        ComboProyecto.SelectedIndex = 0
    End Sub
    Private Sub InitializeTimer()
        Timer1.Enabled = True
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

        'Si la hora de inicio esta marcada entonces
        Static Temp_Seg As Long

        If LTrim(txtIHoraInicio.Text) <> "" Then

            Dim HoraInicio As String
            Dim HoraFin As String
            Dim ReturnValue As TimeSpan
            Dim H As DateTime
            Dim H2 As DateTime
            Dim tot As String

            ' incrementa
            Temp_Seg = Temp_Seg + 1
            ' comprueba que los segundos no sea igual a la cantidad de minutos _ que queremos , en este caso 5 minutos
            If (Temp_Seg * 60) >= (INTERVALO_EN_MINUTOS * 60) * 60 Then
                ' reestablece
                Temp_Seg = 0

                HoraInicio = Format(txtIHoraInicio.Text, "Long Time")
                HoraFin = Format(DateTime.Now, "Long Time")
                H = Format(HoraFin, "long time")
                H2 = Format(HoraInicio, "long time")
                ReturnValue = H.Subtract(H2)
        

                tot = Microsoft.VisualBasic.Right("00" & ReturnValue.Hours, 2) & ": Horas " & _
                    Microsoft.VisualBasic.Right("00" & ReturnValue.Minutes, 2) & ": Minutos " & _
                    Microsoft.VisualBasic.Right("00" & ReturnValue.Seconds, 2) & ". Segundos " & _
                Microsoft.VisualBasic.Right("000" & ReturnValue.Milliseconds, 3)
                'MsgBox("Tiempo trabajado  en el proyecto " & ComboProyecto.Text & " " & tot & "  ", MsgBoxStyle.Information)
                settings.ShowBalloonTip(1000, "Tiempo trabajado en el proyecto  " & ComboProyecto.Text & " " & tot & "  ", "informacion", ToolTipIcon.Info)
            End If


        End If


       
    End Sub

    Private Sub btonInicio_Click(sender As Object, e As EventArgs)

        Try
            If Trim(ComboProyecto.Text) = "" Then
                MsgBox("Debes incluir el proyecto por el cual trabajaras", vbInformation)
                ComboProyecto.Focus()
                Exit Sub

            End If
            ' If Trim(comboUsuario.Text) = "" Then
            'MsgBox("Debes incluir el usuario", vbInformation)
            'comboUsuario.Focus()
            'Exit Sub
            'End If
            txtIHoraInicio.Text = DateTime.Now
            btonInicio.Enabled = False
            btonFin.Enabled = True
            ' Me.WindowState = FormWindowState.Minimized
        Catch ex As Exception
            MsgBox(Err.Description, vbCritical)
        End Try


    End Sub

    Private Sub btonFin_Click(sender As Object, e As EventArgs)

        Dim HoraInicio As String
        Dim HoraFin As String
        Dim ReturnValue As TimeSpan
        Dim H As DateTime
        Dim H2 As DateTime
        Dim date1 As Date
        Dim date2 As Date
        Dim CadenaC As String


        date1 = txtIHoraInicio.Text
        date1 = date1.ToString("G")

        Try
            HoraInicio = Format(txtIHoraInicio.Text, "Long Time")
            HoraFin = Format(DateTime.Now, "Long Time")
            date2 = DateTime.Now
            date2 = date2.ToString("G")


            H = Format(HoraFin, "long time")
            H2 = Format(HoraInicio, "long time")
            ReturnValue = H.Subtract(H2)
            txtHoraFin.Text = ReturnValue.Hours
            txtHoraFin.Text = ReturnValue.ToString("c")


            lblTiempo.Text = Microsoft.VisualBasic.Right("00" & ReturnValue.Hours, 2) & ": Horas " & _
                 Microsoft.VisualBasic.Right("00" & ReturnValue.Minutes, 2) & ": Minutos " & _
                 Microsoft.VisualBasic.Right("00" & ReturnValue.Seconds, 2) & ". Segundos " & _
                Microsoft.VisualBasic.Right("000" & ReturnValue.Milliseconds, 3)
            MsgBox("Tiempo trabajado  " & lblTiempo.Text & "  ", MsgBoxStyle.Information)

            SalirToolStripMenuItem1.Enabled = True



            CadenaC = "Data Source=spbclsql01\opko;Initial Catalog=TimeTracking;Persist Security Info=True;User ID=TimeTracking;Password=1bZTN7Z5"

            Dim sqlCon = New SqlConnection(CadenaC)
            Dim sqlComm As New SqlCommand()
            sqlComm.Connection = sqlCon
            sqlComm.CommandText = "InsertHorasProy"
            sqlComm.CommandType = CommandType.StoredProcedure

            sqlComm.Parameters.AddWithValue("NombreProyecto", ComboProyecto.Text)
            ' sqlComm.Parameters.AddWithValue("Usuario", comboUsuario.Text)
            sqlComm.Parameters.AddWithValue("Usuario", lblUsuario.Text)
            sqlComm.Parameters.AddWithValue("HoraIni", date1)
            sqlComm.Parameters.AddWithValue("HoraFin", date2)
            sqlComm.Parameters.AddWithValue("TotalHoras", ReturnValue.ToString("c"))
            sqlCon.Open()

            sqlComm.ExecuteNonQuery()
            sqlCon.Close()
            SalirToolStripMenuItem1.Enabled = True
            MsgBox("Registro incluido satisfactoriamente", MsgBoxStyle.Exclamation)

            txtIHoraInicio.Text = ""
            txtHoraFin.Text = ""
            lblTiempo.Text = ""

            btonFin.Enabled = False
            btonInicio.Enabled = True

        Catch ex As Exception
            MsgBox("el error es " & ex.Message)

        End Try

    End Sub

    Private Sub Label5_Click(sender As Object, e As EventArgs)

    End Sub


    Private Sub Salir_Click(sender As Object, e As EventArgs) Handles Salir.Click
        Application.Exit()
    End Sub

    Private Sub Form1_move(sender As Object, e As EventArgs) Handles MyBase.Move
        ' If Me.WindowState = FormWindowState.Minimized Then
        'Me.Hide()
        'settings.ShowBalloonTip(1000, "Notificacion esta minimizando la ventana", "informacion", ToolTipIcon.Info)
        'End If
    End Sub

    Private Sub ContextMenuStrip1_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ContextMenuStrip1.Opening

    End Sub


    Private Sub mostrar_Click(sender As Object, e As EventArgs) Handles mostrar.Click
        Me.Show()

    End Sub

    Private Sub settings_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles settings.MouseDoubleClick
        Me.Show()

    End Sub

    Private Sub SalirToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem1.Click
        Application.Exit()

    End Sub

    Private Sub frmInicio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SalirToolStripMenuItem1.Enabled = False
        ' Imports System.DirectoryServices
        'Imports System.DirectoryServices.ActiveDirectory
        'Imports System.DirectoryServices.AccountManagement
        'Imports System.DirectoryServices.AccountManagement



        '        Label1.Text = System.Security.Principal.WindowsIdentity.GetCurrent().Name ' usuario acttual de windos
        '       Label2.Text = System.Security.Principal.WindowsIdentity.GetCurrent().NameClaimType   ' usuario acttual de windos

        lblUsuario.Text = Mid(System.Security.Principal.WindowsIdentity.GetCurrent().Name, 6)


    End Sub

    Private Sub MenuStrip1_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles MenuStrip1.ItemClicked

    End Sub

    Private Sub StatusStrip1_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs)

    End Sub

    Private Sub btonInicio_Click_1(sender As Object, e As EventArgs) Handles btonInicio.Click
        Try
            If Trim(ComboProyecto.Text) = "" Then
                MsgBox("Debes incluir el proyecto por el cual trabajaras", vbInformation)
                ComboProyecto.Focus()
                Exit Sub

            End If
            ' If Trim(comboUsuario.Text) = "" Then
            'MsgBox("Debes incluir el usuario", vbInformation)
            'comboUsuario.Focus()
            'Exit Sub
            'End If
            txtIHoraInicio.Text = DateTime.Now
            btonInicio.Enabled = False
            btonFin.Enabled = True
            ' Me.WindowState = FormWindowState.Minimized
        Catch ex As Exception
            MsgBox(Err.Description, vbCritical)
        End Try
    End Sub

    Private Sub btonFin_Click_1(sender As Object, e As EventArgs) Handles btonFin.Click
        Dim HoraInicio As String
        Dim HoraFin As String
        Dim ReturnValue As TimeSpan
        Dim H As DateTime
        Dim H2 As DateTime
        Dim date1 As Date
        Dim date2 As Date
        Dim CadenaC As String


        date1 = txtIHoraInicio.Text
        date1 = date1.ToString("G")

        Try
            HoraInicio = Format(txtIHoraInicio.Text, "Long Time")
            HoraFin = Format(DateTime.Now, "Long Time")
            date2 = DateTime.Now
            date2 = date2.ToString("G")


            H = Format(HoraFin, "long time")
            H2 = Format(HoraInicio, "long time")
            ReturnValue = H.Subtract(H2)
            txtHoraFin.Text = ReturnValue.Hours
            txtHoraFin.Text = ReturnValue.ToString("c")


            lblTiempo.Text = Microsoft.VisualBasic.Right("00" & ReturnValue.Hours, 2) & ": Horas " & _
                 Microsoft.VisualBasic.Right("00" & ReturnValue.Minutes, 2) & ": Minutos " & _
                 Microsoft.VisualBasic.Right("00" & ReturnValue.Seconds, 2) & ". Segundos " & _
                Microsoft.VisualBasic.Right("000" & ReturnValue.Milliseconds, 3)
            MsgBox("Tiempo trabajado  " & lblTiempo.Text & "  ", MsgBoxStyle.Information)

            SalirToolStripMenuItem1.Enabled = True



            CadenaC = "Data Source=spbclsql01\opko;Initial Catalog=TimeTracking;Persist Security Info=True;User ID=TimeTracking;Password=1bZTN7Z5"

            Dim sqlCon = New SqlConnection(CadenaC)
            Dim sqlComm As New SqlCommand()
            sqlComm.Connection = sqlCon
            sqlComm.CommandText = "InsertHorasProy"
            sqlComm.CommandType = CommandType.StoredProcedure

            sqlComm.Parameters.AddWithValue("NombreProyecto", ComboProyecto.Text)
            ' sqlComm.Parameters.AddWithValue("Usuario", comboUsuario.Text)
            sqlComm.Parameters.AddWithValue("Usuario", lblUsuario.Text)
            sqlComm.Parameters.AddWithValue("HoraIni", date1)
            sqlComm.Parameters.AddWithValue("HoraFin", date2)
            sqlComm.Parameters.AddWithValue("TotalHoras", ReturnValue.ToString("c"))
            sqlCon.Open()

            sqlComm.ExecuteNonQuery()
            sqlCon.Close()
            SalirToolStripMenuItem1.Enabled = True
            MsgBox("Registro incluido satisfactoriamente", MsgBoxStyle.Exclamation)

            txtIHoraInicio.Text = ""
            txtHoraFin.Text = ""
            lblTiempo.Text = ""

            btonFin.Enabled = False
            btonInicio.Enabled = True

        Catch ex As Exception
            MsgBox("el error es " & ex.Message)

        End Try
    End Sub
End Class
